import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsplayerComponent } from './detailsplayer.component';

describe('DetailsplayerComponent', () => {
  let component: DetailsplayerComponent;
  let fixture: ComponentFixture<DetailsplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
