import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptteamsComponent } from './apptteams.component';

describe('ApptteamsComponent', () => {
  let component: ApptteamsComponent;
  let fixture: ComponentFixture<ApptteamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApptteamsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApptteamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
