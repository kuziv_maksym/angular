import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgserviceService } from '../ngservice.service';
import { Team } from '../team';


@Component({
  selector: 'app-updateteam',
  templateUrl: './updateteam.component.html',
  styleUrls: ['./updateteam.component.css']
})
export class UpdateteamComponent implements OnInit {

  id: number;
  team: Team;

  constructor(private route: ActivatedRoute,private router: Router,
    private _service: NgserviceService) { }

  ngOnInit() {
    this.team = new Team();

    this.id = this.route.snapshot.params['id'];
    
    this._service.getTeam(this.id)
      .subscribe(data => {
        console.log(data)
        this.team = data;
      }, error => console.log(error));
  }

  updateTeam() {
    this._service.updateTeam(this.id, this.team)
      .subscribe(data => {
        console.log(data);
        this.team = new Team();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateTeam();    
  }

  gotoList() {
    this.router.navigate(['/team']);
  }
}