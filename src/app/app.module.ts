import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppplayersComponent } from './appplayers/appplayers.component';
import { AppplayerComponent } from './appplayer/appplayer.component';
import { AppteamComponent } from './appteam/appteam.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApptteamsComponent } from './apptteams/apptteams.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CreateteamComponent } from './createteam/createteam.component';
import { AppRoutingModule } from './app-routing.module';
import { NgserviceService } from './ngservice.service';
import { UpdateteamComponent } from './updateteam/updateteam.component';
import { TeamdetailsComponent } from './teamdetails/teamdetails.component';
import { ApplayersComponent } from './applayers/applayers.component';
import { UpdateplayerComponent } from './updateplayer/updateplayer.component';
import { AddplayerComponent } from './addplayer/addplayer.component';
import { DetailsplayerComponent } from './detailsplayer/detailsplayer.component';

@NgModule({
  declarations: [
    AppComponent,
    AppplayersComponent,
    AppplayerComponent,
    AppteamComponent,
    ApptteamsComponent,
    CreateteamComponent,
    UpdateteamComponent,
    TeamdetailsComponent,
    ApplayersComponent,
    UpdateplayerComponent,
    AddplayerComponent,
    DetailsplayerComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
