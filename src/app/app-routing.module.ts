import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ApptteamsComponent } from './apptteams/apptteams.component';
import { CreateteamComponent } from './createteam/createteam.component';
import { UpdateteamComponent } from './updateteam/updateteam.component';
import { TeamdetailsComponent } from './teamdetails/teamdetails.component';
import { AppplayersComponent } from './appplayers/appplayers.component';
import { AddplayerComponent } from './addplayer/addplayer.component';



const routes: Routes = [
  { path: '', redirectTo: 'team', pathMatch: 'full' },
  { path: 'team', component:  ApptteamsComponent},
  { path: 'add', component: CreateteamComponent },
  { path: 'edit/:id', component: UpdateteamComponent },
  { path: 'details/:id', component: TeamdetailsComponent },
  { path: 'players/:id', component: AppplayersComponent },
  { path: 'player/add', component: AddplayerComponent },
];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
