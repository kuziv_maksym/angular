import { Component, OnInit } from '@angular/core';
import { NgserviceService } from '../ngservice.service';
import { Team } from '../team';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-apptteams',
  templateUrl: './apptteams.component.html',
  styleUrls: ['./apptteams.component.css']
})
export class ApptteamsComponent implements OnInit {

   
    teams: Observable<Team[]>;

  constructor(private _service: NgserviceService,
    private router: Router) { }

  ngOnInit(){
  	this.reloadData();
  }

 reloadData() {
    this._service.getTeams().subscribe(data => {
      this.teams =data;
    });
  }

  deleteTeam(id: number) {
    this._service.deleteTeam(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  updateTeam(id: number) {
    this.router.navigate(['edit', id]);
  }

  detailsTeam(id: number){
    this.router.navigate(['details',id]);
  }

  detailsTeamPlayers(id: number) {
    this.router.navigate(['players',id]);
  }


}
