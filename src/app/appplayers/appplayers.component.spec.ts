import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppplayersComponent } from './appplayers.component';

describe('AppplayersComponent', () => {
  let component: AppplayersComponent;
  let fixture: ComponentFixture<AppplayersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppplayersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppplayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
