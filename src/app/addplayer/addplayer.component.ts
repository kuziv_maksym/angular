import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgserviceService } from '../ngservice.service';
import { Player } from '../player';

@Component({
  selector: 'app-addplayer',
  templateUrl: './addplayer.component.html',
  styleUrls: ['./addplayer.component.css']
})
export class AddplayerComponent implements OnInit {

  player: Player = new Player();
  submitted = false;

  constructor(private _service: NgserviceService,
    private router: Router) { 
    this.player = new Player();
  }

  ngOnInit() {
  }

  newPlayer(): void {
    this.player = new Player();
    this.submitted = false;
  }

  save() {
    this._service
    .createPlayer(this.player).subscribe( data => {
      console.log(data);
      }, 
    error => console.log(error));
  }

  onSubmit() {
    console.log(this.player);  
    this.save();
  }

  gotoList(id: number) {
    this.router.navigate(['players',id]);
  }
}