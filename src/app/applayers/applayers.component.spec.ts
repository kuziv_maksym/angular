import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplayersComponent } from './applayers.component';

describe('ApplayersComponent', () => {
  let component: ApplayersComponent;
  let fixture: ComponentFixture<ApplayersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplayersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
