import { Team } from "./team";

export class Player {
    id: number;
    firstname: string;
    lastname: string;
    experience: number;
    startDate: string;
    age: number;
    price: number;
    team: Team;
}
