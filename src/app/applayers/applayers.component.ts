import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgserviceService } from '../ngservice.service';
import { Player } from '../player';

@Component({
  selector: 'app-applayers',
  templateUrl: './applayers.component.html',
  styleUrls: ['./applayers.component.css']
})
export class ApplayersComponent implements OnInit {

  id: number;
  players: Observable<Player[]>;

  constructor(private route: ActivatedRoute, private _service: NgserviceService,
    private router: Router) { }

  ngOnInit(): void {


    this.id = this.route.snapshot.params['id'];
    this.reloadData(this.id);
    }

    reloadData(id: number) {
      this._service.getPlayersByTeamId(id).subscribe(data => {
        this.players = data;
      });
    }
  }