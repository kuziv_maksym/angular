export class Team {
	id: number;
	city: string;
	name: string;
	comission: number;
	budget: number;
}
