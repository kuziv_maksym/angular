import { Component, OnInit } from '@angular/core';
import { Team } from '../team';
import { Router } from '@angular/router';
import { NgserviceService } from '../ngservice.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-teamdetails',
  templateUrl: './teamdetails.component.html',
  styleUrls: ['./teamdetails.component.css']
})
export class TeamdetailsComponent implements OnInit {

  id: number;
  team: Team;

  constructor(private route: ActivatedRoute,private router: Router,
    private _service: NgserviceService) { }

  ngOnInit() {
    this.team = new Team();

    this.id = this.route.snapshot.params['id'];
    
    this._service.getTeam(this.id)
      .subscribe(data => {
        console.log(data)
        this.team = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['/team']);
  }
}