import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from './team';
import { Player } from './player';

@Injectable({
  providedIn: 'root'
})
export class NgserviceService {

  constructor(private _http:HttpClient) { }

  getTeams():Observable<any>{
  	return this._http.get<any>(`http://localhost:8080/api/team`);
  }

  createTeam(team: Team): Observable<Object>{
  return this._http.post(`http://localhost:8080/api/team`, team);
  }


  updateTeam(id: number, value: any): Observable<Object> {
    return this._http.put(`http://localhost:8080/api/team/${id}`, value);
  }

  getTeam(id: number): Observable<any> {
    return this._http.get(`http://localhost:8080/api/team/${id}`);
  }

   deleteTeam(id: number): Observable<any> {
    return this._http.delete(`http://localhost:8080/api/team/${id}`, { responseType: 'text' });
  }

  getPlayersByTeamId(id: number): Observable<any> {
    return this._http.get<any>(`http://localhost:8080/api/team/players/${id}`);
  }

  createPlayer(player: Player): Observable<Object>{
    return this._http.post(`http://localhost:8080/api/team/player`, player);
  }



}
