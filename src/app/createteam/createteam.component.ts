import { Component, OnInit } from '@angular/core';
import { Team } from '../team';
import { Router } from '@angular/router';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-createteam',
  templateUrl: './createteam.component.html',
  styleUrls: ['./createteam.component.css']
})
export class CreateteamComponent implements OnInit {


  team: Team = new Team();
  submitted = false;

  constructor(private _service: NgserviceService,
    private router: Router) { 
    this.team = new Team();
  }

  ngOnInit() {
  }



  save() {
    this._service
    .createTeam(this.team).subscribe( data => {
      console.log(data)
      this.gotoList();
      }, 
    error => console.log(error));
  }

  onSubmit() {
    console.log(this.team);  
    this.save();
  }

  gotoList() {
    this.router.navigate(['/team']);
  }
}
